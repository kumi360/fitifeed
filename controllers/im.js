var https = require('https');
var http = require('http');
var dir = './public/image_test/';
var url = require('url');
var fs = require('fs');
var im = require('imagemagick');
var _request;
var _response;
var _inputImagePath = dir + "test.jpg";
var _outputImagePath = dir + "out.png";
var sys = require('util');
//var font_path = "/Library/Fonts/";
//var font_path = "/Users/kumi/Library/Fonts/";
var font_path = module.parent.font_path;

var processImage = function(inputImagePath, inputImageProperties, text, outputImagePath, request, callback){
	outHtml = "";
	inputImageProperties = inputImageProperties.split('x');
	//inputImagePath
	//console.log('converting image');
	im.convert([
		//'-blur', '5x05',
		//'-colorize', '50,0,50',
		'-size', inputImageProperties[0] + 'x' + inputImageProperties[1],
		'-background', 'transparent',
		'\(',
			'-size', inputImageProperties[0] + 'x' + inputImageProperties[1],
			//'-background', 'RGBA(0,0,0, 0.2)',
			'caption:',
		'\)',
		'\(',
			'-size', inputImageProperties[0] + 'x' + inputImageProperties[1],
			'-gravity', 'Center',
			'-font', font_path + 'peatinapay.ttf',
			'-fill', '#FFFFFF',
			'-background', 'transparent',
			'caption:'+text,
		'\)',
		'-flatten',
		outputImagePath], function(err, stdout, stderr){
		if(err){
			//console.log('error converting: ' + err); 
			throw err;
		}
		else{
			//console.log('stdout: ' + stdout);
			//console.log('converted');
			data = {
				pinurl: 'http://' + request.headers.host,
				pinmedia: 'http://' + request.headers.host + "/public/image_test/out.png",
				text: text,
				inputImageUrl: "http://" + _request.headers.host + "/public/image_test/test.jpg?r=_"+Math.floor(Math.random()*10000),
				outputImageUrl: "http://" + _request.headers.host + "/public/image_test/out.png?r=_"+Math.floor(Math.random()*10000),
				width: inputImageProperties[0],
				height: inputImageProperties[1]
			}
			callback(data);
		}
	});
}

var getImageInfo = function(imagePath, callback){
	im.identify(['-format', '%wx%h', imagePath], function(err, value){
		if(!err){
			callback(value);
		}else{
			console.log('error: ' + err);
			response.write('error: '+ err);
			response.end();
		}
	});
}

var saveImage = function(request, response, imageData, callback){
	fs.writeFile(_inputImagePath, imageData, function(err) {
	    if(err) {
	        console.log(err);
	        response.end();
	    } else {
	    	callback(_inputImagePath);
	    }
	});
}
this.showImage = function(request, response, image_url, text){
	//Default image
	_request = request;
	_response = response;
	if(!image_url)
		image_url = 'http://media-cache3.pinterest.com/upload/35958496994743870_vszHX7Ts_f.jpg';
	if(!text)
		text = "given how we agree life is a miracle we sure do bitch about it a lot";
	if(request.body.txtImageUrl)
		image_url = request.body.txtImageUrl;
	if(request.body.txtText)
		text = request.body.txtText;

	  var image_host_name = url.parse(image_url).hostname;
	  var filename = url.parse(image_url).pathname.split("/").pop();
	
	  var http_client = http.createClient(80, image_host_name);
	  var image_get_request = http_client.request('GET', image_url, {"host": image_host_name});
	  image_get_request.addListener('response', function(proxy_response){
	    var current_byte_index = 0;
	    var response_content_length = parseInt(proxy_response.header("Content-Length"));
	    var response_body = new Buffer(response_content_length);
	   
	    proxy_response.setEncoding('binary');
	    proxy_response.addListener('data', function(chunk){
	      response_body.write(chunk, current_byte_index, "binary");
	      current_byte_index += chunk.length;
	    });
	    proxy_response.addListener('end', function(){
	      //response.contentType(filename);
	      //response.send(response_body);
	      saveImage(request, response, response_body, function(imagePath){
	      	//inputImagePath = dir + 'test.jpg';
	      	//outputImagePath = dir + 'out.png';
	      	getImageInfo(_inputImagePath, function(inputImageProperties){
	      		processImage(_inputImagePath, inputImageProperties, text, _outputImagePath, request, function(data){
		      		response.render('showImage.ejs', data);
		      	});
	      	});
	      });
	    });
	  });
	  image_get_request.end();
}

