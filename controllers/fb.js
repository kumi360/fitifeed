this.fbAuthLand = function(request, response){
	//response.write('called back from fb');
	//response.end();
	response.render("fbland.ejs");
}

this.fbAuthSubmit = function(request, response){
	var token = request.body.fbAuthToken.split('&');
	fbToken = token[0];
	var redirect = request.body.redirectUrl;
	module.parent.models.fbconnect.getFBUser(fbToken, function(d) {
		fbUser = JSON.parse(d);
	    var eduEmail = undefined;
	    if(!fbUser.email){
	    	response.redirect('/');
	    }else{
	    	module.parent.models.user.findUserByEmail(fbUser.email, function(error, result){
	    		if(!error){
	    			if(!result){
	    				//New user
	    				module.parent.models.userThreshold.getThresholdSpaceLeft(function(err, spaceLeft){
				    		if(!err){
								if(spaceLeft > 0){
									module.parent.controllers.fb.findCreateFBUser(token, fbUser, redirect, response);
							    }else{
				    				module.parent.controllers.invite.addInviteRequest(fbUser.email, response, function(err){});
				    				//Signup for invite request
									response.write('Thanks for your interest, the registration is closed right now but we have automatically added you to the waiting list. Thank you for your interest. -Team Fiti');
			    					response.end();
							    }
						   }
					   });
	    			}else{
	    				//Existing user
	    				module.parent.controllers.fb.findCreateFBUser(token, fbUser, redirect, response);
	    			}
	    		}
	    	});
		 }
	});
}

this.findCreateFBUser = function(token, fbUser, redirect, response){
	var userModel = module.parent.models.user;
	userModel.findOrCreateFBUser(
    	fbUser.first_name,
    	fbUser.last_name,
    	fbUser.name,
    	fbUser.email,
    	undefined,
    	"http://graph.facebook.com/" + fbUser.id + "/picture",
    	"http://graph.facebook.com/" + fbUser.id + "/picture?type=large",
    	"fb",
    	fbUser.id,
    	function(err, result){
    		if(!err){
    			if(result._id){
    				///Now get all their friends asyncly and add to db
    				//Add only if new user..
    				userModel.addAllFBFriends(result._id, token);
    				//Set the user cookie here
    				userModel.setUserCookie(result, response);
					if(redirect == undefined || redirect.length == 0 || !redirect){
						response.redirect('/home');
					}
					else{
						response.redirect(redirect);
					}
    			}else{
    				//console.log('error: ' + JSON.stringify(result));
    				response.write('Error: ' + JSON.stringify(result));
    				response.end();
    			}
    		}else{
    			
    		}
    	}
    );
}