var getUser = function(request, response){
	return module.parent.models.user.getUserCookie(request, response);
}

this.testCurl = function(request, response){
	var sys = require('util')
	var exec = require('child_process').exec;
	//function puts(error, stdout, stderr) { sys.puts(stdout) }
	exec("curl -o img.jpg https://encrypted-tbn3.google.com/images?q=tbn:ANd9GcTOMgQeBqKa_qi1zkT3B81ExzqI_DSKRT-RM36ZO2TMVWn4Ujq5", function(error, stdout, stderr){
		console.log('got data back from curl');
		console.log('Error: ' + error);
		console.log('Stdout: ' + stdout);
		console.log('stderr: ' + stderr);
		if(!error){
			console.log('Dumping to S3');
			module.parent.controllers.im2.dumpToS3('img.jpg', 'public/images/test.jpg', function(err){
				console.log('Finished');
				/*cleanUp(image);
				if(callback){
					if(err)
						callback(err);
					else
						callback(null);
				}*/
			});
		}
	});
	response.end('done');
}

this.ses = function(request, response){
	var AmazonSES = require('./ses.js');
	var ses = new AmazonSES(module.parent.awsAccessId, module.parent.awsSecretKey);
	//var out = ses.verifyEmailAddress('no-reply@fitifeed.com');
	//console.log('Submited for verification');
	/*ses.listVerifiedEmailAddresses(function(result) {
	    console.log(result);
	});*/
	/*ses.send({
	      from: 'no-reply@fitifeed.com'
	    , to: ['nr_anand@yahoo.com']
	    , subject: 'Test subject'
	    , body: {
	          text: 'This is the text of the message.'
	        , html: 'This is the html body of the message.'
	    }
	});
	response.end('Sent email');*/
	response.render('request_invite.ejs', {}, function(err, html){
		console.log(html);
	});
	console.log('got html');
	response.end();
}

this.land = function(request, response){
	user = getUser(request, response);
	if(user)
		response.redirect('/home');
	else{
		//response.render('home.ejs');
		module.parent.models.userThreshold.getThresholdSpaceLeft(function(err, spaceLeft){
			if(!err){
				data = {
					redirect: '',
					spaceLeft: spaceLeft,
					fbSecret: module.parent.fbSecret
				}
				response.render('user_login.ejs', data);
			}
		});
	}
}
this.filter = function(request, response){
	if(request.params.url){
		filtered = module.parent.controllers.filters.addFilter(request.params.url, 'out2.png', request.body.txtCommand, function(err, data){
			//response.write('out: ' + data);
			if(!err){
				data = {
					filters:  {
						command: request.body.txtCommand,
						image1: decodeURIComponent(request.params.url),
						image2: data.url,
						image3: "",
						image4: "",
						image5: ""
					}
				}
				response.render('filters.ejs', data);
			}else{
				console.log(err);
				response.end();
			}
		});
	}else{
		response.write('no url');
		response.end();
	}
}

this.showImage = function(request, response){
	if(request.params.url){
		//if(!text)
		fonts = module.parent.controllers.im2.fonts();
		colors = module.parent.controllers.im2.colors();
		var text = 'Default caption';
		data = {
			pinurl: 'http://' + request.headers.host,
			//pinurl: request.params.url,
			pinmedia: '',
			inboundUrl: '',
			text: text,
			inputImageUrl: request.params.url,
			outputImageUrl: '',
			width: '',
			height: '',
			fonts: fonts,
			colors: colors
		}
		response.render('showImage2.ejs', data);
		//module.parent.controllers.im2.showImage(request, response, request.params.url);
	}
	else{
		response.write('no url');
		response.end();
	}
}

this.getImage = function(request, response){
	var tw = request.body.tw;
	var th = request.body.th;
	var x = request.body.x;
	var y = request.body.y;
	var text = request.body.text;
	var font_id = request.body.font_id;
	var color_id = request.body.color_id;
	//console.log(request.body);
	//module.parent.controllers.im2.getTextImage
	params = {
		request: request, 
		tw: tw, 
		th: th, 
		text: text,
		font_id: font_id,
		color_id: color_id
	};
	module.parent.controllers.im2.getTextImage(params, function(err, url){
		response.setHeader("Content-Type", "text/html");
		if(!err){
			response.write(url);
			response.end();
		}else{
			response.write('error: ' + err);
			response.end();
		}
	});
}

this.mergeImage = function(request, response){
	var imgUrl = request.body.txtSourceUrl; //'http://media-cache6.pinterest.com/upload/195484440045089019_CBXfVErR_f.jpg'
	var x = '+' + request.body.txtX;
	var y = '+' + request.body.txtY;
	var text = request.body.txtText;
	if(imgUrl && x && y){
		module.parent.controllers.im2.mergeOutput(
			imgUrl,
			x,y,'out1.png',
			function(err, out){
				if(!err){
					var url = 'http://' + request.headers.host + out.url;
					//response.redirect('/share/' + url);
					data = {
						imageUrl: url,
						pinurl: "http://fitifeed.com",
						text: text
					};
					response.render('share.ejs', data);
				}else{
					response.header('Content-Type', 'text/html');
					response.write('error downloading the image from the url: ' + imgUrl + 
					'<br><br>Please be kind enough to drop me an email on this error with the page url '+
					'<br>to: nr.anand@gmail.com '+
					'<br>many thanks!!!!!');
					response.end();
				}
			}
		);
	}else{
		response.write('page not found');
		response.end();
	}
}



this.genFonts = function(request, response){
	/*module.parent.controllers.im2.getTextImage(request, x, y, tw, th, text, function(err, url){
		
	});*/
	fonts = module.parent.controllers.im2.fonts();
	
	for(i=0; i<fonts.length; i++){
		console.log('Gen: ' + fonts[i].name);
		params = {
			request: request,
			tw: '150',
			th: '50',
			text: fonts[i].name,
			outimageName: i+'.png',
			font_id: i,
			color_id: 1
		};
		module.parent.controllers.im2.getTextImage(params, function(err, url){
			console.log(err);
			console.log(url);
		});
	}
	response.end('done');
	/*params.text = 'Papyrus';
	params.outimageName = '2.png';
	params.font_id = 2;
	module.parent.controllers.im2.getTextImage(params, function(err, url){
		console.log(err);
		console.log(url);
	});
	params.text = 'Zapfino';
	params.outimageName = '5.png';
	params.font_id = 5;
	module.parent.controllers.im2.getTextImage(params, function(err, url){
		console.log(err);
		console.log(url);
	});
	params.text = 'Monaco';
	params.outimageName = '3.png';
	params.font_id = 3;
	module.parent.controllers.im2.getTextImage(params, function(err, url){
		console.log(err);
		console.log(url);
	});
	params.text = 'RosewoodStd';
	params.outimageName = '4.png';
	params.font_id = 4;
	module.parent.controllers.im2.getTextImage(params, function(err, url){
		console.log(err);
		console.log(url);
	});*/
	//console.log(fonts.length);
	//response.end();
}