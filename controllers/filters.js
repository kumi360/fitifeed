var im = require('imagemagick');
var dir = './public/image_test/';
var dirUrl = "/public/image_test/";
var font_path = module.parent.font_path;
this.addFilter = function(sourceImageUrl, destImageName, command, callback){
	//console.log(sourceImageUrl);
	module.parent.controllers.im2.downloadImage(sourceImageUrl, 'download.jpg', function(err, sourceImagePath){
		if(err)
			callback(err, null);
		else{ 
			im.convert([
				sourceImagePath,
				'\(', '+clone', '-alpha', 'extract', '-virtual-pixel', 'black', 
	             '-spread', '80', '-blur', '0x3', '-threshold', '50%', '-spread', '10', '-blur', '0x.7',
	             '-paint', '5', 
	             '\)',
	          	'-alpha', 'off', 
				'-sepia-tone', '90%',				
				'-compose', 'Copy_Opacity', '-composite',
				'\(',
					//'-region', '100x10%+0+0',
					//'-size', '100x100',
					//'caption:some words that needs to be here and it looks great',
					//'-morphology', 'Convolve', 'Gaussian:0x3',
					//'-blur', '5x5',
					//'-equalize',
					//'-flatten',
				'\)',
				dir + destImageName], function(err, stdout, stderr){
					if(callback){
						if(err)
							callback(err);
						else{
							im.convert([
								dir + destImageName,
								'\(',									
									'-blur', '3x2',
									'-size', '500x200+20+20',
									'-gravity', 'South',
									'-font', font_path+'peatinapay.ttf',
									'-fill', '#FFFFFF',
									'-background', 'transparent',
									//'-background', 'RGBA(0,0,0,0.2)',
									'caption:%[fx:w]Some caption that goes here',
								'\)',
								//'-geometry',  '+10+20',
								'-composite',
								dir + destImageName], function(err, stdout, stderr){
									if(callback){
										if(err)
											callback(err);
										else{
											outPutImage = {
												path: dir + destImageName,
												url: dirUrl + destImageName + "?r=_" + Math.floor(Math.random()*100000)
											};
											callback(null, outPutImage);
										}
									}
								});
						}
					}
			});
		}
	});
}