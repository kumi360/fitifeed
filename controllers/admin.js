var AmazonSES = require('./ses.js');
var ses = new AmazonSES(module.parent.awsAccessId, module.parent.awsSecretKey);
/* 
 * Admin section
 */
this.inviteRequests = function(request, response){
	module.parent.models.inviteme.getAllInvites(function(err, requests){
		if(!err){
			data = {
				requests: requests
			};
			response.render('admin/showInvites.ejs', data);
		}else{
			response.end('Error: ' + err);
		}
	});
}

this.handleAction = function(request, response){
	if(request.body.action == 'approve'){
		approveRequest(request, response);
	}else if(request.body.action == 'delete'){
		deleteRequest(request, response);
	}else{
		response.end('Bad request');
	}
}

var deleteRequest = function(request, response){
	requests = request.body.requests;
	var ids = new Array; 
	for(i=0;i<requests.length;i++){
		module.parent.models.inviteme.deleteInvite(requests[i]);
	}
	response.end('success');
}

var approveRequest = function(request, response){
	requests = request.body.requests;
	var ids = new Array; 
	var emails = new Array;
	for(i=0;i<requests.length;i++){
		ids.push(requests[i].id);
		emails.push(requests[i].email);
	}
	module.parent.models.inviteme.markApproved(ids, function(err){
		if(!err){
			response.end('success');
			//now async sending emails
			for(i=0;i<emails.length;i++){
				if(module.parent.sendEmails){
					response.render('emails/invite_sent.ejs', {id: requests[i].id}, function(err, html){
						var out = html.split('|');
						ses.send({
						      from: module.parent.noreplyEmail
						    , to: [requests[i].email]
						    , subject: out[0]
						    , body: {
						        html: out[1]
						    }
						});
					});
				}
			}
		}else{
			response.end('Error: ' + err);
		}
	});
	//Send emails in a seperate thread
}
