var getUser = function(request, response){
	return module.parent.models.user.getUserCookie(request, response);
}
this.home = function(request, response){
	user = getUser(request, response);
	if(!user)
		response.redirect('/');
	else{
		module.parent.models.fiti.getLatestFitis(0, 50, function(err, fitis){
			data = {
				fitis:	fitis
			};
			response.render('new_home.ejs', data);
		});
	}
}
this.logout = function(request, response){
	module.parent.models.user.clearUserCookie(response);
	//response.redirect("/");
	response.end('logged out');
}
this.login = function(request, response){
	user = getUser(request, response);
	if(user)
		response.redirect('/home');
	else{
		var redirect = undefined;
		if(request.params.redirect && request.params.url)
			redirect = "/" + request.params.redirect + "/" + encodeURIComponent(request.params.url);
		//if(redirect)
			//redirect = request.params.split('cropImage')
		module.parent.models.userThreshold.getThresholdSpaceLeft(function(err, spaceLeft){
			if(!err){
				data = {
					redirect: redirect,
					spaceLeft: spaceLeft,
					fbSecret: module.parent.fbSecret
				}
				response.render('user_login.ejs', data);
			}
		});
	}
}
