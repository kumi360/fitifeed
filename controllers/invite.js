var AmazonSES = require('./ses.js');
var ses = new AmazonSES(module.parent.awsAccessId, module.parent.awsSecretKey);

this.requestInvite = function(request, response){
	var email = request.body.txtInviteMe;
	//addInviteRequest(email);
	module.parent.controllers.invite.addInviteRequest(email, response, function(err){
		if(!err)
			response.end('success');
		else
			response.end('error: ' + err);
	});
}

this.addInviteRequest = function(email, response, callback){
	if(validateEmail(email)){
		module.parent.models.inviteme.requestInvite(email, function(err){
			if(!err){
				//Send email to the email address
				if(module.parent.sendEmails){
					response.render('emails/request_invite.ejs', {}, function(err, html){
						var out = html.split('|');
						ses.send({
						      from: module.parent.noreplyEmail
						    , to: [email]
						    , subject: out[0]
						    , body: {
						        html: out[1]
						    }
						});
						if(callback)
							callback(null);
					});
				}
			}else{
				if(callback)
					callback(err);
			}
		});
	}else{
		if(callback)
			callback('Invalid email');
	}
}
var validateEmail = function(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

this.acceptInvite = function(request, response){
	var inviteId = request.params.id;
	//response.end('got it');
	module.parent.models.inviteme.findInviteById(inviteId, function(err, invite){
		if(!err){
			if(invite && invite.approved){
				data = {
					redirect: '',
					spaceLeft: 0,
					fbSecret: module.parent.fbSecret
				}
				response.render('approved_login.ejs', data);
			}else{
				response.send(404);
			}
		}else{
			if(err == "Error: Invalid ObjectId"){
				response.send(404);
			}
		}
	});
}

this.fbAuthSubmitAcceptInvite = function(request, response){
	var inviteId = request.params.id;
	module.parent.models.inviteme.findInviteById(inviteId, function(err, invite){
		if(!err){
			if(invite && invite.approved){
				var token = request.body.fbAuthToken.split('&');
				fbToken = token[0];
				module.parent.models.fbconnect.getFBUser(fbToken, function(d) {
					fbUser = JSON.parse(d);
					//Delete the invite
					module.parent.models.inviteme.deleteInvite(inviteId, function(err){
						//Create user
						module.parent.controllers.fb.findCreateFBUser(token, fbUser, undefined, response);
					});
				});
			}else{
				response.send(404);
			}
		}
	});
}