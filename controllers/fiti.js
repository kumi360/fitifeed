var getUser = function(request, response){
	return module.parent.models.user.getUserCookie(request, response);
}
this.showFiti = function(request, response){
	//user = getUser(request, response);
	var fitiid = request.params.id;
	module.parent.models.fiti.getFitiInfoById(fitiid, function(err, fiti){
		if(!err){
			if(fiti){
				data = {
					fiti		: fiti,
					url			: 'http://fitifeed.com/fiti/' + fiti._id,
					image_url	: 'http://fitifeed.com/public' + fiti.path,
					fbAppID		: module.parent.fbSecret
				}
				response.render('fiti_land.ejs', data);
			}else{
				response.send(404);
			}
		}
	});
}
