var fs = require('fs');
var utils = require('./utils');

var MailDomainSigner = require('./MailDomainSigner');

var private_key = fs.readFileSync(__dirname + '/../../config/key.priv');
var associatedDomain = "dashlane.com";

var quoted_printable_encode = utils.quoted_printable_encode;
var trim = utils.trim;

function RawEmail() {
    var headers = {};
    headers.mimever = 'MIME-Version: 1.0';
    headers.ctype = 'Content-Type: multipart/alternative; charset=UTF-8; boundary="' + boundary + '"';

    var textBody = "";
    var htmlBody = "";

    function makeBoundary() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for ( var i = 0; i < 60; i++)
            text += possible.charAt(Math.floor(Math.random() * 62));

        return text;
    }

    function getBody(){
        var body = "";
        body += '--' + boundary + "\n";
        body += 'Content-Type: text/plain; charset=ISO-8859-1\n';
        body += 'Content-Transfer-Encoding: quoted-printable' + "\n\n";
        body += quoted_printable_encode(trim(textBody.replace(/\s+/g, ' '))) + "\n";
        body += '--' + boundary + "\n";
        body += 'Content-Type: text/html; charset=ISO-8859-1\n';
        body += 'Content-Transfer-Encoding: quoted-printable' + "\n\n";
        body += quoted_printable_encode(trim(htmlBody.replace(/\s+/g, ' '))) + "\n";
        body += '--' + boundary + "--" + "\n";

        return body;
    }

    var boundary = '-----=' + makeBoundary();

    this.setReturnPath = function(returnPathAddress){
        headers.returnPath = 'Return-Path: <' + returnPathAddress + '>';
    };

    this.setSender = function(sender) {
        var fromHeader = "From: ";
        if (sender.email && sender.name) {
            fromHeader +=  sender.name + ' <' + sender.email + '>';
        } else {
            fromHeader += sender;
        }
        headers.from = fromHeader;
    };

    this.setReceivers = function(receivers) {
        var toHeader = "To: ";
        if(!Array.isArray(receivers)){
            receivers = [receivers];
        }
        for ( var receiverIndex = 0; receiverIndex < receivers.length; receiverIndex++) {
            var receiver = receivers[receiverIndex];
            if (receiver.email && receiver.name) {
                toHeader += receiver.name + ' <' + receiver.email + '>, ';
            } else {
                toHeader += receiver + ', ';
            }
        }
        headers.to = toHeader.substring(0, toHeader.length - 2);
    };

    this.setCcReceivers = function(receivers) {
        var toHeader = "Cc: ";
        if(!Array.isArray(receivers)){
            receivers = [receivers];
        }
        for ( var receiverIndex = 0; receiverIndex < receivers.length; receiverIndex++) {
            var receiver = receivers[receiverIndex];
            if (receiver.email && receiver.name) {
                toHeader += '"' + receiver.name + '"' + ' <' + receiver.email + '>, ';
            } else {
                toHeader += receiver + ', ';
            }
        }
        headers.cc = toHeader.substring(0, toHeader.length - 2);
    };

    this.setSubject = function(subject) {
        subject = trim(subject.replace(/\s+/g, ' '));
        subject = new Buffer(subject).toString('base64');
        subject = '=?utf-8?B?' + subject + '?=';

        headers.subject = "Subject: " + subject;
    };

    this.setText = function(text) {
        textBody = text;
    };

    this.setHtml = function(html) {
        htmlBody = html;
    };

    this.getEncodedMessage = function() {
        var body = getBody();

        var mailDomainSigner = new MailDomainSigner(private_key, associatedDomain, "dkim1");

        var dkim_sign = mailDomainSigner.getDKIM("from:to:subject:mime-version:content-type:",
                                [headers.from, headers.to, headers.subject, headers.mimever, headers.ctype],
                                body);

        var email = dkim_sign + "\r\n";

        for (var header in headers) {
            email += headers[header] + "\r\n";
        }

        email += "\r\n" + body;

        return new Buffer(email).toString('base64');
    };

    this.getSource = function(){
        var from = 'From: ';
        var index = headers.from.indexOf(from);
        if(index !== -1){
            return headers.from.substring(index + from.length);
        }
        else {
            return headers.from;
        }
    };
}

module.exports = RawEmail;
