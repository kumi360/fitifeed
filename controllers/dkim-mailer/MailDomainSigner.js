var crypto = require('crypto');

function MailDomainSigner(_pkid, _d, _s){
    this.pkid = _pkid;
    this.d = _d;
    this.s = _s;

    function trim(str) {
        str += '';
        var l = str.length;
        var i = 0;
        var whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";

        for (i = 0; i < l; i++) {
            if (whitespace.indexOf(str.charAt(i)) === -1) {
                str = str.substring(i);
                break;
            }
        }

        l = str.length;
        for (i = l - 1; i >= 0; i--) {
            if (whitespace.indexOf(str.charAt(i)) === -1) {
                str = str.substring(0, i + 1);
                break;
            }
        }

        return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
    }

    function rtrim (str) {
        var charlist = '\r\n';
        var re = new RegExp('[' + charlist + ']+$', 'g');
        return (str + '').replace(re, '');
    }

    function wordwrap (str, int_width, str_break, cut) {
        // Wraps buffer to selected number of characters using string break char  
        // 
        // version: 1109.2015
        // discuss at: http://phpjs.org/functions/wordwrap
        // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
        // +   improved by: Nick Callen
        // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Sakimori
        // +   bugfixed by: Michael Grier
        // *     example 1: wordwrap('Kevin van Zonneveld', 6, '|', true);
        // *     returns 1: 'Kevin |van |Zonnev|eld'
        // *     example 2: wordwrap('The quick brown fox jumped over the lazy dog.', 20, '\n');
        // *     returns 2: 'The quick brown fox \njumped over the lazy\n dog.'
        // *     example 3: wordwrap('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');
        // *     returns 3: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod \ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \ncommodo consequat.'
        // PHP Defaults
        var m = ((arguments.length >= 2) ? arguments[1] : 75);
        var b = ((arguments.length >= 3) ? arguments[2] : "\n");
        var c = ((arguments.length >= 4) ? arguments[3] : false);
     
        var i, j, l, s, r;
     
        str += '';
     
        if (m < 1) {
            return str;
        }
     
        for (i = -1, l = (r = str.split(/\r\n|\n|\r/)).length; ++i < l; r[i] += s) {
            for (s = r[i], r[i] = ""; s.length > m; r[i] += s.slice(0, j) + ((s = s.slice(j)).length ? b : "")) {
                j = c == 2 || (j = s.slice(0, m + 1).match(/\S*(\s)?$/))[1] ? m : j.input.length - j[0].length || c == 1 && m || j.input.length + (j = s.slice(m).match(/^\S*/)).input.length;
            }
        }
     
        return r.join("\n");
    }

    function chunk_split (body, chunklen, end) {
        // Returns split line  
        // 
        // version: 1109.2015
        // discuss at: http://phpjs.org/functions/chunk_split
        // +   original by: Paulo Freitas
        // +      input by: Brett Zamir (http://brett-zamir.me)
        // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Theriault
        // *     example 1: chunk_split('Hello world!', 1, '*');
        // *     returns 1: 'H*e*l*l*o* *w*o*r*l*d*!*'
        // *     example 2: chunk_split('Hello world!', 10, '*');
        // *     returns 2: 'Hello worl*d!*'
        chunklen = parseInt(chunklen, 10) || 76;
        end = end || '\r\n';
     
        if (chunklen < 1) {
            return false;
        }
     
        return body.match(new RegExp(".{0," + chunklen + "}", "g")).join(end);
    }


    function headRelaxCanon(s){
       s = s.replace(/\r\n\s+/g, " ");
       var lines = s.split('\r\n');

       for (var i = 0; i < lines.length; i++) {
           var line = lines[i];
           var firstColon = line.indexOf(':');
           var heading = line.substring(0, firstColon);
           var value = line.substring(firstColon + 1);

           heading = heading.toLowerCase();
           value = value.replace(/\s+/g, " ");

           lines[i] = heading + ":" + trim(value);
       }

       s = lines.join('\r\n');
       return s;
    }

    function bodyRelaxCanon(body){
        if(body === ''){
            return '\r\n';
        }

        body = body.replace(/\r\n/g, '\n');
        body = body.replace(/\n/g, '\r\n');

        while(body.substring(body.length - 2) === "\r\n"){
          body = body.substring(0, body.length - 2);
        }

       return body + "\r\n";
    }
    
    /**
    * DKIM-Signature Header Creator Function implementation according to RFC4871
    */
    this.getDKIM = function(h, _h, body){
        var _b = bodyRelaxCanon(body);
        var _l = _b.length;

        var shasum = crypto.createHash('sha1');
        shasum.update(_b);

        var _bh = shasum.digest('base64');

        var _dkim = "DKIM-Signature: " +
                "v=1; " +                 // DKIM Version
                "a=rsa-sha1; " +          // The algorithm used to generate the signature "rsa-sha1"
                "s=" + this.s + "; " +    // The selector subdividing the namespace for the "d=" (domain) tag
                "d=" + this.d + "; " +    // The domain of the signing entity
                "l=" + _l + "; " +        // Canonicalizated Body length count
                "c=relaxed/relaxed; " +   // Message (Headers/Body) Canonicalization "relaxed/relaxed"
                "h=" + h + "; " +         // Signed header fields
                "bh=" + _bh + ";\r\n\t" + // The hash of the canonicalized body part of the message
                "b=";                     // The signature data (Empty because we will calculate it later)


        // Wrap DKIM Header
        _dkim = wordwrap(_dkim, 76, "\r\n\t");
        
        // Canonicalization Header Data
        var _unsigned  = headRelaxCanon(_h.join("\r\n") + "\r\n" + _dkim);

        var signer = crypto.createSign("sha1");
        signer.update(_unsigned);

        var _signed = signer.sign(this.pkid, 'base64');
        
        // Base64 encoded signed data
        // Chunk Split it
        // Then Append it $_dkim
        _dkim   += chunk_split(_signed,76,"\r\n\t");
        
        // Return trimmed $_dkim
        return trim(_dkim);

    };
}

module.exports = MailDomainSigner;