/*

Heavily inspired by the PHP implementation made by Ahmad Amarullah (available at http://code.google.com/p/php-mail-domain-signer/), with the help of http://phpjs.org/.

Setup:

In dkim-raw-email.js, change the location of your private key and the associatedDomain accordingly to your needs.

Example of use (using aws-lib, https://github.com/mirkok/aws-lib):

*/

    var aws = require("aws-lib");
    var sesCredentials = require("./../../config/credentials").ses;
    var ses = aws.createSESClient(sesCredentials.AWSAccessKeyId, sesCredentials.AWSSecretKey);

    var RawEmail = require('./dkim-raw-email');

    function sendRawEmail(from, to, cc, subject, HTMLbody, Textbody, cb){
        var senderEmail = {email: 'no-reply@dashlane.com', name: 'Dashlane'};

        var rawEmail = new RawEmail();
        rawEmail.setReturnPath('contact@dashlane.com');
        rawEmail.setSender(senderEmail);
        rawEmail.setSubject(subject);
        rawEmail.setReceivers(to);

        if(cc){
            rawEmail.setCcReceivers(cc);
        }

        rawEmail.setText(Textbody);
        rawEmail.setHtml(HTMLbody);

        ses.call(
            'SendRawEmail',
            {
              'RawMessage.Data' : rawEmail.getEncodedMessage(),
              'Source' : rawEmail.getSource()
            },
            function(result) {
              if (result.Error !== undefined) {
                cb(result.Error);
              } else {
                cb(null, result);
              }
            }
        );
    }