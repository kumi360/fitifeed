var im = require('imagemagick');
var dir = '.' + module.parent.uploadFolder;
var dirUrl = module.parent.uploadFolder;
var tempCropImage = module.parent.tempCropImage;
var cropImageUrl = tempCropImage + "?r=_"+Math.floor(Math.random()*10000);
var filteredImage = module.parent.tempFilteredImage;
var tempTextImage = module.parent.tempTextImage;
var finalOutputImage = module.parent.finalOutImage;
var url = require('url');
var getUser = function(request, response){
	return module.parent.models.user.getUserCookie(request, response);
}
this.crop = function(request, response){
	user = getUser(request, response);
	if(user){
		url = request.params.url;
		source = (request.params.source?request.params.source:"");
		//url.parse(//request.headers)
		fonts = module.parent.controllers.im2.fonts();
		data = {
			text: "Default Caption",
			fonts: fonts,
			sourceImage: url,
			sourceUrl:source 
		};
		response.render('crop.ejs', data);
	}else{
		response.redirect('/login' + request.url);
	}
}
this.cropTheImage = function(request, response){
	shrinkRation = request.body.shrinkRatio;
	sourceImage = request.body.sourceImageUrl;
	x1 = request.body.x1 * shrinkRation;
	y1 = request.body.y1 * shrinkRation;
	//x2 = request.body.x2;
	//y2 = request.body.y2;
	width = request.body.width * shrinkRation;
	height = request.body.height * shrinkRation;
	var user = getUser(request);
	module.parent.controllers.im2.downloadImage(sourceImage, user["id"] + "_" + tempCropImage, function(err, data){
		if(!err){
			im.convert([
				data,
				'-crop',
				width + 'x' + height + '+' + x1 + '+' + y1,
				data], function(err, stdout, stderr){
					if(err){
						console.log('error|' + err);
						response.end();
					}
					else{						
						if(width > 600){
							//resize
							im.convert([
								data,
								'-resize',
								'600x',
								data], function(err, stdout, stderr){
									if(!err){
										finishCroping(data, request, response);
									}else{
										console.log('Error');
										response.write('error|Error resizing: ' + err);
										response.end();
									}
								});
						}else{
							finishCroping(data, request, response);
						}
					}
			});
		}else{
			response.write('error|' + err);
			response.end();
		}
	});
}
var finishCroping = function(image, request, response){
	var user = getUser(request);
	module.parent.controllers.im2.dumpToS3(image, image, function(err){
		cleanUp(image);
		if(err)
			console.log('error: ' + err);
		else{
			response.write('http://' + request.headers.host + dirUrl + user["id"] + "_" + cropImageUrl);
			response.end();
		}
	});
}
var cleanUp = function(file, cleanS3, callback){
	var fs = require('fs');
	fs.unlink(file, function (err) {
		if(callback){
			if (err) 
				callback(err);
			else
				callback(null);
		}
	});
	if(cleanS3){
		module.parent.controllers.im2.deleteS3File(file);
	}
}
this.getTextImage = function(request, response){
	var tw = request.body.tw;
	var th = request.body.th;
	var x = request.body.x;
	var y = request.body.y;
	var text = request.body.text;
	var font_id = request.body.font_id;
	var color_id = request.body.color_id;
	var textDirection = (request.body.textDirection?request.body.textDirection:'Center');
	//text = text.replace('\n', '');
	//text = autoLineBreak(text);
	var user = getUser(request);
	var fontSize = calculateFontSize(text, tw, th);
	params = {
		request: request, 
		tw: tw, 
		th: th, 
		text: text,
		font_id: font_id,
		color_id: color_id,
		outimageName: user["id"] + "_" + tempTextImage,
		textDirection: textDirection,
		fontSize: fontSize
	};
	//console.log('total words: ' + countWords(text) + ' width: ' + tw + ' height: ' + th);
	module.parent.controllers.im2.getTextImage(params, function(err, url){
		response.setHeader("Content-Type", "text/html");
		if(!err){
			response.write('success|' + url);
			response.end();
		}else{
			response.write('error|' + err);
			response.end();
		}
	});
}
var autoLineBreak = function(text){
	lineCount = text.split('\n');
	newText = "";
	lineLimit = 23;
	for(i=0; i<lineCount.length; i++){
		if(lineCount[i].length < lineLimit && lineCount[i].length > 2){
			//console.log('len: ' + lineCount[i].length + ' added: ' + newText);
			newText += lineCount[i] + '\n';
			//console.log('text: ' + newText);
		}
		else if(lineCount[i].length > 1){
			newText += lineCount[i].substring(0, lineLimit);
			newText += '\n' + lineCount[i].substring(lineLimit+1, lineCount[i].length);
		}
	}
	//Remove the last lineBreak if there is one
	newText = newText.replace(/\n$/,'');
	//console.log(newText);
	return newText;
}
var calculateFontSize = function(text, width, height){
	//console.log(text);
	//text = text.replace(/(\r\n|\n|\r)/gm,"");
	//console.log(text);
	//console.log(lineCount);
	//console.log(lineCount[0].length);
	var low = width / 368 * 45;
	var high = width / 368 * 100;
	//console.log(low  + " | " + high);
	//console.log(text);
	//count = countWords(text);
	//ratio = width / height;
	//size = width / 350 * (4/text.length*40);
	//size = parseInt(count/2 * size);
	//console.log((368/15) + " | " + width/text.length);
	//halfText = text.length;
	size = (((width/text.length)*(8/368))*((width/text.length)*(8/368))*high);
	//console.log('obtained size: ' + size);
	if(size < low)
		size = low;
	else if(size > high)
		size = high;
	size = parseInt(size)
	//console.log(text.length);
	/*console.log("width: " + width + " height: " + height + 
		" count: " + text.length + " fontsize: " + size + "\n" +
		" low: " + low + " high: " + high
		);*/
	return size;
	//return 30;
	
	//for width 368
	//lowest 30 highest 50
	//return 30;
}
var countWords = function(text){
	var splitWords = text.split(' ');
	return splitWords.length;
}
this.finalizeImage = function(request, response){
	var user = getUser(request);
	//Create new fiti here
	width = request.body.width;
	height = request.body.width;
	text = request.body.text;
	sourceImageUrl = request.body.sourceImageUrl;
	source_url = request.body.sourceUrl;
	baseImage = request.body.baseImage;
	module.parent.models.fiti.createNewFiti(text, user["id"], source_url, sourceImageUrl, function(err, fiti){
		if(!err){
			var image;
			if(baseImage != "none")
				image = dir + user["id"] + "_" + filteredImage;
			else
				image = dir + user["id"] + "_" + tempCropImage;
			textImage = dir + user["id"] + "_" + tempTextImage;
			var _outImage = fiti["_id"] + ".png";
			var _localOutPath = dir + _outImage;
			var _s3OutImage = user["id"] + "/" + _outImage;
			var _s3OutPath = dir + _s3OutImage;
			module.parent.controllers.im2.getFileFromS3(image, function(err, filename){
				if(!err){
					module.parent.controllers.im2.getFileFromS3(textImage, function(err, filename){
						if(!err){
							im.convert([
								image,
								'-size', width + 'x' + height,
								'-background', 'transparent',
								'\(',
									textImage,
									//'-geometry',  x + y,
								'\)',
								'-composite',
								_localOutPath], function(err, stdout, stderr){
									outputUrl = 'http://' + request.headers.host + dirUrl + _s3OutImage + "?r=_"+Math.floor(Math.random()*10000);
									if(!err){
										var path = _s3OutPath.replace('\/', '/').replace('./', '/').replace('public/', '');
										module.parent.models.fiti.updatePath(fiti["_id"], path);
										module.parent.controllers.im2.dumpToS3(_localOutPath, _s3OutPath, function(err){
											if(!err){
												module.parent.models.fiti.activateFiti(fiti["_id"]);
												cleanUp(_localOutPath);
												cleanUp(textImage);
												cleanUp(image);
												data = {
													imageUrl: outputUrl,
													pinurl: "http://fitifeed.com/fiti/"+fiti["_id"],
													text: text
												};
												response.render('share.ejs', data);
											}
										});
									}
									else
										response.end('error:' + err);
								}
							);
						}else{
							console.log('err finalizing (getFileFromS3 textImage) file: ' + textImage + ' err: ' + err);
						}
					});
				}else{
					console.log('err finalizing (getFileFromS3 image): img: ' + image + ' err: ' + err);
				}
			});
		}else{
			console.log('err finalizing (createNewFiti): ' + err);
		}
	});
}
this.applyFilter = function(request, response){
	//image = request.body.croppedImage;
	//-colorspace Gray
	var user = getUser(request);
	filter = request.params.filter;
	image = dir + user["id"] + "_" + tempCropImage;
	var _filteredImage = user["id"] + "_" + filteredImage;
	
		var imParams;
		var filterSet = false;
		outImage = dir + _filteredImage;
		if(filter == 'gray'){
			imParams = [
				image,
				'-colorspace', 'Gray',
				'-contrast',
				outImage];
			filterSet = true;
		}
		else if(filter == 'sepia'){
			imParams = [
				image,
				'-sepia-tone', '80%',
				outImage];
			filterSet = true;
		}
		else if(filter == 'burn'){
			imParams = [
				image,
				//'-modulate', '100,100,166.6',
				//'-modulate', '100,100,200',
				//'-color-matrix', "' 0 0 1 0 1 0 1 0 0 '",
				//'+level-colors', 'Firebrick,',
				'-contrast', '-modulate', '100,150,100', '-auto-gamma',
				outImage];
			filterSet = true;
		}
		else if(filter == 'firebrick'){
			imParams = [
				image,
				'+level-colors', 'Firebrick,',
				outImage];
			filterSet = true;
		}
		if(filterSet){
			module.parent.controllers.im2.getFileFromS3(image, function(err, filename){
				im.convert(imParams, function(err, stdout, stderr){
					if(err){
						console.log('error|' + err);
						response.end();
					}
					else{
						module.parent.controllers.im2.dumpToS3(outImage, outImage, function(err){
							if(!err){
								cleanUp(image);
								cleanUp(outImage);
								response.write('http://' + request.headers.host + dirUrl + _filteredImage + "?r=_" + Math.floor(Math.random()*100000));
								response.end();
							}else{
								console.log('error|' + err);
								response.end();
							}
						});
					}
				});
			});
		}else{
			response.write('http://' + request.headers.host + dirUrl + user["id"] + "_" + tempCropImage + "?r=_" + Math.floor(Math.random()*100000));
			response.end();
		}
		//N -negate D   -compose ColorBurn -composite  -negate  R
}