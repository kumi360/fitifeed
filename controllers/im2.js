var https = require('https');
var http = require('http');
var dir = '.' + module.parent.uploadFolder;
var dirUrl = module.parent.uploadFolder;
var url = require('url');
var fs = require('fs');
var im = require('imagemagick');
var _request;
var _response;
var _inputImagePath = dir + "test.jpg";
var _outputImagePath = dir + "out.png";
var _outputTextImageName = "text.png";
var sys = require('util');
var font_path = module.parent.font_path;
var default_font = '0';
var default_color = '0';
var outImageUrl = dirUrl + "out.png?r=_"+Math.floor(Math.random()*10000);
var knox = require('knox');
var client = knox.createClient({
    key: module.parent.awsAccessId
  , secret: module.parent.awsSecretKey
  , bucket: module.parent.awsBucket
});

var getUser = function(request){
	return module.parent.models.user.getUserCookie(request);
}
var getWorkingFolder = function(request){
	//console.log('getting working folder: ');
	user = getUser(request);
	return dir + user["id"] + module.parent.workingFolder;
}

this.getTextImage = function(params, callback){
	request = params.request;
	w = params.tw;
	h = params.th;
	text = params.text;
	outimageName = (params.outimageName ? params.outimageName : undefined); 
	params.font_id = (params.font_id?params.font_id:default_font);
	
	if(this.fonts()[params.font_id] == undefined)
		params.font_id = default_font;
	font = this.fonts()[params.font_id].font;
	
	params.color_id = (params.color_id?params.color_id:default_color);
	if(this.colors()[params.color_id] == undefined)
		params.color_id = default_color;
	color = this.colors()[params.color_id].code;
	
	
	var user = getUser(request);
	if(outimageName == '' || outimageName == undefined || outimageName == null)
		outimageName = _outputTextImageName;
	
		
	var outTextImageUrl = dirUrl + outimageName + "?r=_"+Math.floor(Math.random()*10000);
	outimageName = dir + outimageName;
	
	var direction = 'Center';
	var fontSizeOption = '';
	var fontSize = '';
	var imParams = ['\(', '-size', w + 'x' + h];
	if(params.textDirection){
		fontSizeOption = '-pointsize';
		if(params.fontSize)
			fontSize = params.fontSize;
		else
			fontSize = '40';
		direction = params.textDirection;
		imParams.push(fontSizeOption);
		imParams.push(fontSize);
	}
	imParams.push('-gravity')
	imParams.push(direction);
	//imParams.push('-stroke');
	//imParams.push("#000C");
	imParams.push('-font');
	imParams.push(font_path+font);
	imParams.push('-fill');
	imParams.push(color);
	imParams.push('-background');
	imParams.push('transparent');
	//'-background', 'RGBA(0,0,0,0.2)',
	imParams.push('caption:'+text);
	imParams.push('\)');
	imParams.push('-flatten');
	imParams.push(outimageName);
		
	im.convert(imParams, function(err, stdout, stderr){
			if(callback){
				if(err){
					console.log('error converting: ' + err);
					//throw err;
					callback(err);
				}
				else{
					finishGetText(outimageName, params.request, function(err){
						if(!err)
							callback(null, 'http://' + request.headers.host + outTextImageUrl);
						else
							callback(err);
					});
				}
			}
	});
}
var finishGetText = function(image, request, callback){
	module.parent.controllers.im2.dumpToS3(image, image, function(err){
		cleanUp(image);
		if(callback){
			if(err)
				callback(err);
			else
				callback(null);
		}
	});
}
var cleanUp = function(file, callback){
	var fs = require('fs');
	fs.unlink(file, function (err) {
		if(callback){
			if (err) 
				callback(err);
			else
				callback(null);
		}
	});
}

var getImageInfo = function(imagePath, callback){
	im.identify(['-format', '%wx%h', imagePath], function(err, value){
		if(callback){
			if(!err){
				callback(null, value);
			}else{
				callback(err);
			}
		}
	});
}

this.getFileFromS3 = function(filename, callback){
	name = filename.replace('public', '');
	client.get(name).on('response', function(res){
	  var current_byte_index = 0;
	  var response_content_length = parseInt(res.header("Content-Length"));
	  var response_body = new Buffer(response_content_length);
	  
	  res.setEncoding('binary');
	  res.on('data', function(chunk){
	  	response_body.write(chunk, current_byte_index, "binary");
	    current_byte_index += chunk.length;
	  });
	  res.on('end', function(){
	  	if(res.statusCode != 404){
		  	fs.writeFile(filename, response_body, function(err) {
			      	if(callback){
					    if(err)
					        callback(err);
					    else{
					    	callback(null, filename);
					    }
					}
				});
		}else{
			callback(res.statusCode + " error getting the image");
		}
	  });
	  res.on('error', function(e) {
			callback("error downloading from s3: " + err);
		    console.error(e);
	  });
	}).end();
}

//New download image using curl instead of HTTP request
this.downloadImage = function(image_url, outImageName, callback){
	var sys = require('util')
	var exec = require('child_process').exec;
	//function puts(error, stdout, stderr) { sys.puts(stdout) }
	exec("curl -o " + dir + outImageName + " " + image_url, function(error, stdout, stderr){
		/*console.log('got data back from curl');
		console.log('Error: ' + error);
		console.log('Stdout: ' + stdout);
		console.log('stderr: ' + stderr);
		if(!error){
			console.log('Dumping to S3');
			module.parent.controllers.im2.dumpToS3('img.jpg', 'public/images/test.jpg', function(err){
				console.log('Finished');
			});
		}*/
		if(callback){
		    if(error)
		        callback(error);
		    else{
		    	callback(null, dir + outImageName);
		    }
		}
	});
}

this.downloadImage1 = function(image_url, outImageName, callback){
	//console.log('out name: ' + outImageName);
	var image_host_name = url.parse(image_url).hostname;
	var filename = url.parse(image_url).pathname.split("/").pop();
	var client = http;
	var pathname = url.parse(image_url).pathname;
	//console.log('url: ' + image_url + ', path: ' + pathname);
	if(image_url.indexOf('https') > -1)
	  	client = https;

	client.get({ 
		host: image_host_name, 
		path: pathname,
		"user-agent": "Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.162 Safari/535.19" 
		}, function(res) {
	  //console.log("req head: " + client.headers);
	  //console.log("statusCode: ", res.statusCode);
	  //console.log("headers: ", res.headers);
	  var current_byte_index = 0;
	  var response_content_length = parseInt(res.header("Content-Length"));
	  var response_body = new Buffer(response_content_length);
	  
	  res.setEncoding('binary');
	  res.on('data', function(chunk) {
	    response_body.write(chunk, current_byte_index, "binary");
	    current_byte_index += chunk.length;
	  });
	  res.on('end', function(){
	  	if(res.statusCode != 404){
		  	//console.log('got the image, writing to: ' + outImageName);
		  	fs.writeFile(dir + outImageName, response_body, function(err) {
			      	if(callback){
					    if(err)
					        callback(err);
					    else{
					    	callback(null, dir + outImageName);
					    }
					}
				});
		}else{
			console.log('error: ');
			console.log("headers: ", res.headers);
			callback(res.statusCode + " error getting the image");
		}
	  })
	
	}).on('error', function(e) {
		callback("error: " + err);
	    console.error(e);
	});
}

this.dumpToS3 = function(filename, s3FilePath, callback){
	//dump the file to s3
	if(!s3FilePath)
		s3FilePath = filename;	
	s3FilePath = s3FilePath.replace('\/', '/').replace('./', '/').replace('public', '');
	client.putFile(filename, s3FilePath, function(err, res){
		if(!err){
	  		if(callback)
	    		callback(null);
	    }else{
	    	if(callback)
	    		callback(err);
	    }
	});
}

this.deleteS3File = function(filename, callback){
	fs.readFile(filename, function(err, buf){
	  filename = filename.replace('public', '');
	  client.del(filename).on('response', function(res){
		  if (204 == res.statusCode) {
	    	if(callback)
	      		callback(null);
		  }else{
		    if(callback)
		    	callback(res.headers);
		  }
	  }).end();
	});
}

this.fonts = function(){
	return [{
			name: 'Peatinapay',
			font: 'peatinapay.ttf',
			image_name: '1.png'
		},
		{
			name: 'Chickenhawk',
			font: 'Chickenhawk.ttf',
			image_name: '2.png'
		},
		{
			name: 'Citrus',
			font: 'Citrus.ttf',
			image_name: '3.png'
		},
		{
			name: 'Ripebold',
			font: 'ripebold.otf',
			image_name: '4.png'
		}
		];
}
this.colors = function(){
	return [{
			name: 'White',
			code: '#FFFFFF'
		},
		{
			name: 'Black',
			code: '#000000'
		}];
}
