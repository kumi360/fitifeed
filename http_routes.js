//console.log('routes');
var app = module.parent.app;
var controllers = module.parent.controllers;

/*
 * Setup all the routes you need here
 */
//Redirect all static requests to S3
//console.log(app.settings.env);
if(app.settings.env == 'production'){
	app.all("/public/*", function(request, response){
		response.redirect(module.parent.cdn + request.params);
	});
}

if(app.settings.env == 'development'){
	app.all("/public/userContent/*", function(request, response){
		response.redirect(module.parent.cdn + 'userContent/' + request.params);
	});
}

/* shout cast url */
app.get("/shout", controllers.shout.shout);
//app.all("/testCurl", controllers.landing.testCurl);

app.all("/", controllers.landing.land);
app.get("/login", controllers.user.login);
app.get("/login/:redirect/:url", controllers.user.login);
app.get("/fbAuthLand", controllers.fb.fbAuthLand);
app.post("/fbAuthSubmit", controllers.fb.fbAuthSubmit);


//Invite system
app.post("/requestInvite", controllers.invite.requestInvite);
app.get("/acceptInvite/:id", controllers.invite.acceptInvite);
app.post("/acceptInvite/:id", controllers.invite.fbAuthSubmitAcceptInvite);

// Admin section
app.get("/invitesRequested", controllers.admin.inviteRequests);
app.post("/invitesRequested", controllers.admin.handleAction);

//Post logged in
app.get("/home", controllers.user.home);
app.get("/logout", controllers.user.logout);
app.get("/fiti/:id", controllers.fiti.showFiti);


app.get('/genFonts', controllers.landing.genFonts);

//Remove this later on
//app.post("/getImage", controllers.landing.getImage);

/* testing crop */
app.get('/cropImage/:url', controllers.crop.crop);
app.post('/cropImage/:url', controllers.crop.cropTheImage);
app.post('/applyFilter/:filter', controllers.crop.applyFilter);
app.post("/getTextImage", controllers.crop.getTextImage);
app.post("/finalize", controllers.crop.finalizeImage);


/* testing */
//app.all('/filter/:url', controllers.landing.filter);

//All other requests
app.all('*', function(req, res){
  //res.send('page not found', 404);
  //response.send(404);
  throw new Error('404');
});

app.error(function(err, req, res, next){
	if (err.message && err.message == '404') {
        res.render('errors/404.jade');
    } else {
        next(err);
    }
});