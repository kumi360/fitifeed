var https = require('https');
var options = {
  host: module.parent.fburl,
  port: 443,
  path: '',
  method: 'GET'
};
	
this.getAllFbFriends = function(token, callback){
	options.path = '/me/friends?type=json&limit=10000&access_token=' + token;
	var req = https.request(options, function(res) {
		var fullResponse = "";
    	res.setEncoding('utf8');
		//console.log("statusCode: ", res.statusCode);
	   	//console.log("headers: ", res.headers);
		res.on('data', function(d) {
			fullResponse += d;
		});
		res.on('end', function(){
			allFriends = JSON.parse(fullResponse);
			//console.log(allFriends.data);
			var fbFriends = allFriends.data;
			callback(fbFriends);
		});
	});
	req.end(); //End fb request
	req.on('error', function(e) {
	});
}

this.getFBUser = function(token, callback){
	options.path = "/me?access_token=" + token;
	var req = https.request(options, function(res) {
		var fullResponse = "";
    	res.setEncoding('utf8');
		//console.log("statusCode: ", res.statusCode);
	   	//console.log("headers: ", res.headers);
		res.on('data', function(d) {
			fullResponse += d;
		});
		res.on('end', function(){
			callback(fullResponse);
		});
	});
	req.end(); //End fb request
	
	req.on('error', function(e) {
	  console.error("error getting fbuser: " + e);
	  response.end();
	});
}
