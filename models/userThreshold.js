var mongoose = module.parent.mongoose;
var mongoose_types = module.parent.mongoose_types;
mongoose_types.loadTypes(mongoose);
var Schema = mongoose.Schema;

var schema = new Schema({
	threshold			: {type: Number,   required: true},
	current_totalUsers	: {type: Number,   required: true},
	created_at 			: {type: Date,     required: false},
	updated_at 			: {type: Date,     required: false}
});
var model = mongoose.model('threshold', schema);

this.getThresholdSpaceLeft = function(callback){
	module.parent.models.user.getTotalUsers(function(err, totalUsers){
		if(!err){
			mongoose.connect(module.parent.db);
			model.findOne({}, function(err, curThreshold){
				mongoose.disconnect();
				if(!err){
					var threshold = parseInt(curThreshold.threshold);
					var threshold_total_users = parseInt(curThreshold.current_totalUsers);
					var spacesLeft = (threshold + threshold_total_users) - totalUsers;
					if(spacesLeft < 0)
						spacesLeft = 0;
					if(callback)
						callback(null, spacesLeft);
				}else{
					if(callback)
						callback(err);
				}
			});
		}else{
			if(callback)
				callback(err);
		}
	});
}



var setThreshold = function(threshold, callback){	
	module.parent.models.user.getTotalUsers(function(err, totalUsers){
		mongoose.connect(module.parent.db);
		model.remove({}, function(err) { 
			var userThreshold = new model({
				threshold			: threshold,
				current_totalUsers	: totalUsers,
				created_at			: new Date(),
				updated_at			: new Date()
			});
			userThreshold.save(function(err){
				console.log('saved');
				mongoose.disconnect();
				if(!err){
					if(callback)
						callback(null, userThreshold);
				}
				else{
					if(callback)
						callback(err);
				}
			});
		});
	});
}
