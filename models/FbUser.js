var mongoose = module.parent.mongoose;
var mongoose_types = module.parent.mongoose_types;

// init
mongoose_types.loadTypes(mongoose);
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.ObjectId;
var Email = mongoose.SchemaTypes.Email;
var Url = mongoose.SchemaTypes.Url;

// models
var FbUser = new Schema({
	//keys are provided by facebook
	id          : {type: String,  required: true},
	name        : {type: String,  required: false},
	first_name  : {type: String,  required: true},
	last_name   : {type: String,  required: true},
	link        : {type: Url,     required: false},
	username    : {type: String,  required: false},
	gender      : {type: String,  required: true},
	timezone    : {type: String,  required: false},
	locale      : {type: String,  required: false},
	verified    : {type: Boolean, required: false},
	updated_time: {type: Date,    required: false},
	//keys that i made up
	avatar_url  : {type: Url,     required: false}
});
module.exports = mongoose.model("FbUser", FbUser);