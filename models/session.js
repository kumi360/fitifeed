var mongoose = module.parent.mongoose;
var mongoose_types = module.parent.mongoose_types;
mongoose_types.loadTypes(mongoose);
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var checkins = new Schema({
    uid				: {type: String,	required: true}, 
    checkin_date	: {type: Date,		required: false}
});

var schema = new Schema({
	name			: {type: String,   required: false},
	by				: {type: String,   required: false},
	school			: {type: String,   required: false},
	pic_url			: {type: String,   required: false},
	isLive			: {type: Boolean,  required: false, default: false},
	checkincount	: {type: Number,   required: false},
	checkins		: [checkins],
	created_at 		: {type: Date,     required: true},
	updated_at 		: {type: Date,     required: true}
});
var model = mongoose.model('session', schema);

this.getSessionForUserId = function(uid, callback){
	mongoose.connect(module.parent.db);
	model.find({}, function(error, result){
		mongoose.disconnect();
		if(!error)
			callback(result);
		else
			callback(error);
	});
}

this.markSession = function(sessionid, uid, isLive, callback){
	mongoose.connect(module.parent.db);
	model.findById(sessionid, function(error, session){
		if(!error){
			session.isLive = isLive;
			session.save(function(err){
				callback(session);
			});
		}else{
			mongoose.disconnect();
			callback(err);
		}
	});
}

this.getSessionInfo = function(sessionid, callback){
	mongoose.connect(module.parent.db);
	//module.parent.models.user.getUserInfoByUid(uid, function(user){
		//var condition = {_id: new ObjectId(sessionid), school: user.school};
		model.findById(sessionid, function(error, result){
			mongoose.disconnect();
			if(!error)
				callback(result);
			else
				callback(error);
		});
	//});
}

this.createSession = function (name, byUid, school, pic_url, callback){
	mongoose.connect(module.parent.db);
	var newSession = new model({
		name		: name,
		by			: byUid,
		created_at	: new Date(),
		updated_at	: new Date()
	});
	newSession.save(function(err){
		mongoose.disconnect();
		if(!err){
			callback(newSession);
		}else{
			callback(err);
		}
	});
}

this.removeSession = function(sessionid, byuid, callback){
	mongoose.connect(module.parent.db);
	model.findById(sessionid, function(error, result){
		if(!error){
			if(result.by == byuid){
				result.remove(function(err){
					mongoose.disconnect();
					if(!err){
						callback(result);
					}
					else
						callback(err);
				});
			}else{
				mongoose.disconnect();
			}
		}else{
			mongoose.disconnect();
			callback(error);
		}
	});
}

/*
 * Check in the user to the session collection
 */
this.checkInUserToSession = function(sessionid, uid, callback){
	mongoose.connect(module.parent.db);
	model.findById(sessionid, function(err, s){
		if(s){
			mongoose.disconnect();
			s.checkins.push({uid: uid, checkin_date: new Date()});
			s.save(function(err){
				console.log('user checkin saved in session');
			});
		}
	});
}


/*
 * Check out the user from the session collection
 */
this.checkOutUserFromSession = function(sessionid, uid, callback){
	mongoose.connect();
	model.findById(sessionid, function(err, s){
		if(s){
			mongoose.disconnect();
			s.checkins.pop({uid: uid});
			s.save(function(err){
				console.log('user checked out from session');
			});
		}
	});
}
