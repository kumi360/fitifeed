// init
var mongoose = module.parent.mongoose;
var mongoose_types = module.parent.mongoose_types;
mongoose_types.loadTypes(mongoose);
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var Email = mongoose.SchemaTypes.Email;
var Url = mongoose.SchemaTypes.Url;

var schema = new Schema({
	first_name	: {type: String,   required: false},
	last_name  	: {type: String,   required: false},
	name		: {type: String,   required: false},
	email      	: {type: Email,    required: false, index: {unique: true}},
	password   	: {type: String,   required: false},
	social_type	: {type: String,   required: true},
	fbuser_id  	: {type: String,   required: false},
	pic_url		: {type: String,   required: false},
	pic_thumb	: {type: String,   required: false},
	pic_large	: {type: String,   required: false},
	pic_type	: {type: String,   required: false},
	created_at 	: {type: Date,     required: true},
	updated_at 	: {type: Date,     required: true},
	fbfriends	: {}
});
var model = mongoose.model('user', schema);

var checkUserPic = function(user){
	if(user.pic_type){
		if(user.pic_type == "fb"){
			user.pic_url = "http://graph.facebook.com/" + user.fbuser_id + "/picture";
			user.pic_large = "http://graph.facebook.com/" + user.fbuser_id + "/picture?type=large";
		}
	}
	return user;
}

this.addAllFBFriends = function(uid, token, callback){
	module.parent.models.fbconnect.getAllFbFriends(fbToken, function(fbFriends){
		if(fbFriends){
			//Got all the friends, so save this into the user collection
			//console.log('adding all the fb friends, total: ' + fbFriends.length);
			mongoose.connect(module.parent.db);
			model.findById(uid, function(err, p){
				if(p){
					p.updated_at = new Date();
					p.fbfriends = fbFriends;
					p.save(function(err){
						mongoose.disconnect();
						// Now auto follow existing friends 
						//		and send email notification of new follower
					});
				}
			});
		}
	});
}

this.getUserInfoByUid = function(uid, callback){
	mongoose.connect(module.parent.db);
	var condition = {
		_id		: new ObjectId(uid)
	}
	var user = model.findOne(condition, function(error, result){
		if(!error){
			user = checkUserPic(result);
			callback(user);
		}
		else
			callback(error);
	});
}

this.findAUser = function(callback){
	mongoose.connect(module.parent.db);
	var users = model.find({}, function(error, result){
		mongoose.disconnect();
		callback(result);
	});
}

this.findUserByEmail = function(email, callback){
	mongoose.connect(module.parent.db);
	model.findOne({email: email}, function(error, result){
		mongoose.disconnect();
		if(!error){
			if(callback) callback(null, result);
		}else{
			if(callback) callback(error);
		}
	});
}

this.findOrCreateFBUser = function(first_name, last_name, name, email,
		password, pic_url, pic_large, pic_type, fbuserid, callback){
	//model.findOne({email: email}, function(error, result){
	module.parent.models.user.findUserByEmail(email, function(error, result){
		if(!result){
			mongoose.connect(module.parent.db);
			//Create new user
			var user = new model({
				first_name	: first_name,
				last_name	: last_name,
				name		: name,
				email		: email,
				password	: password,
				pic_url		: pic_url,
				pic_large	: pic_large,
				pic_type	: pic_type,
				social_type	: "fb",
				fbuser_id	: fbuserid,
				created_at	: new Date(),
				updated_at	: new Date()
			});
			user.save(function(err){
				mongoose.disconnect();
				if(!err){
					user = checkUserPic(user);
					callback(null, user);
				}
				else{
					callback(err);
				}
			});
		}else{
			//mongoose.disconnect();
			//Pull existing user
			user = checkUserPic(result);
			callback(null, user);
		}
	});
}

this.getTotalUsers = function(callback){
	mongoose.connect(module.parent.db);
	model.count({}, function(error, result){
		mongoose.disconnect();
		if(!error)
			callback(null, result);
		else
			callback(error);
	});
}

this.createWebUserLoginToken = function(user){
	//Create the cookie token for Web browsers
	var token = user._id + '|' + user.name + '|' + user.first_name + '|' + user.pic_url+ '|' + user.pic_large;
	return module.parent.models.utils.encrypt(token);
}

this.setUserCookie = function(user, response){
	var cookie = this.createWebUserLoginToken(user);
	var cookieDate = new Date();
	cookieDate.setDate(cookieDate.getDate() + 30);
	response.cookie(module.parent.userCookieName, cookie, { expires: cookieDate, httpOnly: true, path: '/' });
}

this.clearUserCookie = function(response){
	response.clearCookie(module.parent.userCookieName);
	response.redirect("/");
}

this.getUserCookie = function(request){
	userCookie = request.cookies[module.parent.userCookieName];
	if(userCookie){
		return this.parseUserCookie(userCookie);
	}else{
		//console.log('no cookie found: ');
		return undefined;
	}
}

this.parseUserCookie = function(encryptCookie){
	token = module.parent.models.utils.decrypt(encryptCookie);
	token = token.split('|');
	userToken = {
		id			: token[0],
		name		: token[1],
		first_name	: token[2],
		pic_url		: (token[3]?token[3]:''),
		pic_large	: (token[4]?token[4]:'')
	};
	return userToken;
}

