var mongoose = module.parent.mongoose;
var mongoose_types = module.parent.mongoose_types;
var ObjectId = mongoose.Schema.ObjectId;

var Schema = mongoose.Schema;
var fiti = new Schema({
	text 	 		: {type: String,     required: true},
	created_by		: {type: String,     required: true},
	source			: {type: String, 	 required: false},
	source_img_url	: {type: String, 	 required: false},
	path			: {type: String, 	 required: false},
	active			: {type: Boolean,	 requred: false},
	created_at   	: {type: Date,       required: true}
});
var model = mongoose.model("fiti", fiti);

this.createNewFiti = function(text, created_by, source_url, source_img_url, callback){
	var fiti = new model({
		text			: text,
		created_by		: created_by,
		source			: source_url,
		source_img_url	: source_img_url,
		active			: false,
		created_at		: new Date()
	});
	mongoose.connect(module.parent.db);
	fiti.save(function(err){
		mongoose.disconnect();
		if(!err){
			callback(null, fiti);
		}else{
			callback(err);
		}
	});
}

this.updatePath = function(fitiid, path, callback){
	mongoose.connect(module.parent.db);
	model.update({_id : fitiid}, {$set: {path: path}}, function(err){
		mongoose.disconnect();
		if(!err)
			if(callback) callback(null);
		else
			if(callback) callback(err);
	});
}

this.activateFiti = function(fitiid, callback){
	mongoose.connect(module.parent.db);
	model.update({_id : fitiid}, {$set: {active: true}}, function(err){
		mongoose.disconnect();
		if(!err)
			if(callback) callback(null);
		else
			if(callback) callback(err);
	});
}

this.getFitiInfoById = function(fitiid, callback){
	mongoose.connect(module.parent.db);
	model.findById(fitiid, function(err, result){
		mongoose.disconnect();
		if(!err){
			if(callback) callback(null, result);
		}else{
			if(callback) callback(err);
		}
	});
}

this.getLatestFitis = function(page, pagesize, callback){
	mongoose.connect(module.parent.db);
	model.find({}).sort('created_at', -1).skip(page*pagesize).limit(pagesize).execFind(function(err, result){
		mongoose.disconnect();
		if(!err){
			if(callback) callback(null, result);
		}else{
			if(callback) callback(err);
		}
	});
}
