// init
var mongoose = module.parent.mongoose;
var mongoose_types = module.parent.mongoose_types;
mongoose_types.loadTypes(mongoose);
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var Email = mongoose.SchemaTypes.Email;
var Url = mongoose.SchemaTypes.Url;

var inviteMe = new Schema({
	email		: {type: Email, required: false},
	created_at	: {type: Date, required: false},
	approved	: {type: Boolean, required: false}
});
var model = mongoose.model('inviteMe', inviteMe);

this.requestInvite = function(email, callback){
	if(email.length > 0){
		mongoose.connect(module.parent.db);
		var condition = {
			email		: email
		}
		var inviteme = model.findOne(condition, function(error, result){
			if(!error){
				if(!result){
					var invite = new model({
						email:	email,
						created_at	: new Date(),
						approved	: false
					});
					invite.save(function(err){
						mongoose.disconnect();
						if(!err){
							if(callback)
								callback(null);
							else
								return 'success';
						}else{
							if(callback)
								callback(err);
							else
								return err;
						}
					});
				}else{
					mongoose.disconnect();
					if(callback)
						callback('email exists');
					else
						return 'error:email exists';
				}
			}
			else
				if(callback)
					callback(error);
				else
					return 'error:' + error;
		});
	}else{
		if(callback)
			callback('Invalid email');
		else
			return 'error:Invalid email';
	}
}


this.deleteInvite = function(inviteId, callback){
	mongoose.connect(module.parent.db);
	model.findById(inviteId, function(error, result){
		if(!error){
			result.remove(function(err){
				mongoose.disconnect();
				if(!err){
					if(callback) callback();
				}
				else{
					if(callback) callback(err);
				}
			});
		}else{
			mongoose.disconnect();
			if(callback) callback(error);
		}
	});
}

this.getAllInvites = function(callback){
	mongoose.connect(module.parent.db);
	model.find({approved : false}).sort('created_at', -1).execFind(function(err, result){
		mongoose.disconnect();
		if(!err)
			callback(null, result);
		else
			callback(err);
	});
}

this.findInviteById = function(inviteid, callback){
	mongoose.connect(module.parent.db);
	model.findById(inviteid, function(err, result){
		mongoose.disconnect();
		if(!err)
			callback(null, result);
		else
			callback(err);
	});
}

this.markApproved = function(requests, callback){
	mongoose.connect(module.parent.db);
	model.update({_id : {$in: requests}}, {$set: {approved: true}}, function(err){
		mongoose.disconnect();
		if(!err)
			if(callback) callback(null);
		else
			if(callback) callback(err);
	});
}
