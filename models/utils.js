var crypto = require('crypto');
var encryption_key = module.parent.encryption_key;
var iv = module.parent.encryption_iv;
this.encrypt = function(plainText){
	var cipher = crypto.createCipheriv('des-ede3-cbc', encryption_key, iv);
	var cryptoString = cipher.update(plainText, 'utf8', 'hex');
	cryptoString += cipher.final('hex');
	return cryptoString;
}


this.decrypt = function(cryptoString){
	var decipher = crypto.createDecipheriv('des-ede3-cbc', encryption_key, iv);
	var plainText = decipher.update(cryptoString, 'hex', 'utf8');
	plainText += decipher.final('utf8');
	return plainText;
}
