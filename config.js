var app = module.parent.app;
var express = module.parent.express;
var fs = require('fs');

//var mongoose = module.parent.mongoose;
/*mongodb = "mongodb://10.176.93.222/starviber";*/
//console.log('from config: ' + module.parent.mongodb);

// configs
app.configure(function() {
	app.set("views", __dirname + "/views");
	//app.set("view engine", "jade");
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	//app.use(express.static(__dirname + "/public"));
	
	//app.use("/public", express.static(__dirname + "/public"));
	
	app.use(express.favicon());
	app.use(express.cookieParser());
	app.use(express.session({secret: "308u534^B#$(^bu3496b3"}));
	app.set('view options', {
		layout: false
	});
	module.parent.fburl = 'graph.facebook.com';
	module.parent.encryption_key = "0987654321zxcr1234567890";
	module.parent.encryption_iv = "31062818";
	module.parent.userCookieName = 'ffuser';
	module.parent.font_path = "./fonts/";
	module.parent.awsAccessId = "AKIAIFCZ56X33E6IFHRQ";
	module.parent.awsSecretKey = "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";
	module.parent.noreplyEmail = '"Fiti Feed" <no-reply@fitifeed.com>';
	module.parent.uploadFolder = "/public/userContent/";
	module.parent.workingFolder = "/working/";
	
	
	module.parent.tempCropImage = 'crop.png';
	module.parent.tempFilteredImage = 'filtered.png';
	module.parent.tempTextImage = 'text.png';
	module.parent.finalOutImage = 'out.png';
});

app.configure("development", function() {
	console.log('loading Dev configs');
	//DB to use
	module.parent.db = "mongodb://ec2-23-22-87-150.compute-1.amazonaws.com/fitifeed-dev";
	module.parent.sendEmails = true;
	app.use(express.errorHandler({
		dumpExceptions: true,
		showStack: true
	}));
	module.parent.fbSecret = "178352335538392";
	module.parent.httpPort = "3000";
	app.use("/public", express.static(__dirname + "/public"));
	module.parent.awsBucket = 'cdn.dev.fitifeed.com';
	//Need to work on fixing the SSL error
	//module.parent.cdn = 'https://' + module.parent.awsBucket + '/';
	module.parent.cdn = 'https://s3.amazonaws.com/' + module.parent.awsBucket + '/';
	//module.parent.cdn = 'http://localhost:3000/public/';
});

app.configure("production", function() {
	console.log('loading Prod configs');
	//Db to use for prod
	module.parent.sendEmails = true;
	//module.parent.db = "mongodb://localhost/fitifeed";
	module.parent.db = "mongodb://10.245.31.59/fitifeed";
	module.parent.fbSecret = "293800967369363";
	app.use(express.errorHandler());
	//module.parent.port = "80";
	module.parent.httpsPort = "443";
	module.parent.httpPort = "80";
	module.parent.awsBucket = 'cdn.fitifeed.com';
	//module.parent.cdn = 'https://' + module.parent.awsBucket + '/';
	//module.parent.cdn = 'http://' + module.parent.awsBucket + '/';
	module.parent.cdn = 'https://s3.amazonaws.com/' + module.parent.awsBucket + '/';
	
	//SSL stuff
	module.parent.sslKey = './ssl/fitifeed.key';
	module.parent.sslCert = './ssl/fitifeed.com.crt';
});

// helper functions
function encryptPassword(password, salt) {
	return crypto.createHmac("sha1", salt).update(password).digest("hex");
}