var appHttps = module.parent.appHttps;
var controllers = module.parent.controllers;

appHttps.all("/public/fitibutton/*", function(request, response){
	//Get the url query string
	q = require('url').parse(request.url).query;
	var red = module.parent.cdn + 'fitibutton/' + request.params + (q?"?"+q:"");
	response.redirect(red);
});
//Redirect all other requests to http
appHttps.all('*', function(req, res){
	res.redirect('http://fitifeed.com'+req.url);
});