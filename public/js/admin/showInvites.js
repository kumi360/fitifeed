$(document).ready(function(){
	
});

function approve(){
	if($('.chkRequest:checked').length > 0){
		request = new Array;
		var curReq = {};
		$('.chkRequest:checked').each(function(){
			curReq["email"] = $('#' + $(this).val() + " .email").text();
			curReq["id"] = $(this).val();
			request.push(curReq);
		});
		data = {
				action: 'approve',
				requests: request
			};
		$.post('', data, function(response) {
			if(response == 'success')
				window.location.reload();
			else
				alert(data);
		});
	}else{
		alert('select atleast one email to approve request');
	}
}

function deleteRequest(){
	if($('.chkRequest:checked').length > 0){
		if(confirm("Are you sure you want to delete these requests?")){
			requests = new Array;
			var curReq = {};
			$('.chkRequest:checked').each(function(){
				requests.push($(this).val());
			});
			data = {
				action: 'delete',
				requests: requests
			};
			$.post('', data, function(response) {
				if(response == 'success')
					setTimeout('window.location.reload()', 500);
				else
					alert(data);
			});
		}
	}else{
		alert('select atleast one email to delete');
	}
}
