$(document).ready(function(){   
    $('.ghost-text').each(function(){
        var d = $(this).val();
        $(this).focus(function(){
            if ($(this).val() == d){
                $(this).val('').removeClass('ghost-text');
            }
        });
        $(this).blur(function(){
            if ($(this).val() == ''){
                $(this).val(d).addClass('ghost-text');
            }
        });
    });
});