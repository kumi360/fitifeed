$(document).ready(function(){
	getQuiz();
	/*question = {
		question	: 'some question',
		options		: {
			"option 1"	: "value",
			"option 2"	: "value"
		}
	}
	showQuizQuestion(question);*/
	//showQuiz();
});
function showAnswers(){
	$('#quizer-parent').animate({marginLeft: -$('.quizer-content').width()}, 200, function(){
		//$('#txtLoginEmail').focus();
	});
}
function questions(){
	$('#quizer-parent').animate({marginLeft: 0}, 200, function(){
		//$('#txtLoginEmail').focus();
	});
}
function askQuestion(){
	$('#overlay').fadeIn(200, function(){
		$(this).bind('click', closeQuestion);
		$('.questionPopup').fadeIn(200, function(){
			
		});
	});
}
function closeQuestion(){
	$('#quizer').fadeOut(50, function(){
		$('#overlay').fadeOut(50);
	});
}
function checkoutClass(){
	$('#data').html('checkoutClass<br>' + $('#data').html());
	inclass.emit('CheckoutClass', $('#txtSessionID').val());
}
function showQuizQuestion(quiz){
	quiz = quiz.quiz;
	if(quiz){
		qHtml = '<div class="quizQuestion">'+ quiz.question + '</div>'+
				'<div class="quizOptions">';
		$.each(quiz.options, function(key, value){
			qHtml += '<div class="options">'+
						key + '. <span>' + value + '</span>'+
					'</div>';
		});
		qHtml += '</div>';
		$('#theQuestion').html(qHtml);
		$('#overlay').fadeIn(200, function(){
			$('.popQuizQuestion').fadeIn(200, function(){
				$('.popQuizQuestion .options').bind('click', function(){
					if($(this).hasClass('selected')){
						$(this).removeClass('selected');
					}else{
						$('.popQuizQuestion .options').removeClass('selected');
						$(this).addClass('selected');
					}
				});
			});
		});
	}
}
function skipQuiz(){
	if(confirm('Are you sure you want to pass on this question?')){
		closePopQuiz();
	}
}
function closePopQuiz(){
	$('.popQuizQuestion').fadeOut(50, function(){
		$('#overlay').fadeOut(50, function(){
			$('#theQuestion').html('');
		});
	});
}
function submitQuiz(){
	if($('.popQuizQuestion .options.selected').length > 0){
		if(inclass){
			inclass.emit('AnswerPopQuiz', $('.popQuizQuestion .options.selected').html());
			closePopQuiz();
		}
	}
	else
		alert('Please select an answer');
}
function getQuiz(){
	//+$('#txtSessionID').val()
  //var urlHost = "http://dev.jumpspoon.com";
  var urlHost = "http://localhost";
  inclass = io.connect(urlHost + '/inclass');

  inclass.on('connect', function (data) {
  	$('#data').html('Connecting to server<br>');
    //inclass.send('hi!');
  })
  .on('connected', function(data){
  	$('#data').html('Connected<br>' + $('#data').html());
  	//$('#data').html('Start syncing..<br>' + $('#data').html());
  	inclass.emit('StartSync', $('#txtSessionID').val());
  	//inclass.emit('pushed popQuiz');
  })
  .on('ProfCheckedout', function(data){
  	window.location = urlHost + ':3000/checkoutSession?id='+$('#txtSessionID').val();
  })
  .on('CloseCheckout', function(data){
  	$('#data').html('Leaving room<br>' + $('#data').html());
  	window.location = urlHost + ':3000/checkoutSession?id='+$('#txtSessionID').val();
  })
  .on('error', function(err){
  	alert('connection refused: ' + JSON.stringify(err));
  	//inclass.disconnect();
  })
  .on('GotAnAnswer', function(answer){
  	//add to the answer totals here
  })
  .on('GotQuizPop', function(data){
  	showQuizQuestion(data);
  })
  .on('PushedPopQuiz', function(data){
	$('#data').html('Pushed to all: ' + JSON.stringify(data) + '<br>' + $('#data').html());
  })
  .on('SomeOneLeft', function(data){
  	if(data.type != 'prof')
  		$('#data').html('Student ' + data.name + ' just left the class<br>' + $('#data').html());
	else
		$('#data').html('Prof ' + data.name + ' just left the class<br>' + $('#data').html());
  })
  .on('SomeOneJoined', function(data){
  	if(data.type != 'prof')
  		$('#data').html('Student ' + data.name + ' just joined the class<br>' + $('#data').html());
	else
		$('#data').html('Prof ' + data.name + ' just joined the class<br>' + $('#data').html());
  });
}
function showQuiz(){
	$('#overlay').fadeIn(200, function(){
		$(this).bind('click', closeQuiz);
		$('#quizer').fadeIn(200, function(){
			
		});
	});
}
function closeQuiz(){
	$('#quizer').fadeOut(50, function(){
		$('#overlay').fadeOut(50);
	});
}

function pushQuestion(){
	try{
		if($('#txtQuizQuestion').val().length > 1){
			question = {
				question	: $('#txtQuizQuestion').val(),
				options		: {}
			}
			i=1;
			$('.quizOptions').each(function(elm){
				if($(this).val().length > 0 && !$(this).hasClass('ghost-text')){
					question.options["" + i + ""] = $(this).val();
					i++;
				}
			});
			//closeQuiz();
			inclass.emit('PushPopQuiz', question);
			/*$('#btnPushQuizNow').hide();
			$('#spnPushQuestionNow').show('fast',function(){
				buildProfQuizAnswers(question);
				setTimeout(function(){
					showAnswers();
				}, 1000);
			});*/
		}else{
			$('#txtQuizQuestion').focus();
		}
	}catch(e){
		alert('error: ' + e);
	}
}

function buildProfQuizAnswers(question){
	html = '<div class="quizAnswerQuestion">'+
				question.question+
			'</div>'+
			'<div class="quizOptions">';
	$.each(question.options, function(key, value){
		html += '<div class="options">'+
			value+
			'<span id="#quizAnswerOption'+key+'">0</span>'+
		'</div>';
	});
	html += '</div>';
	$('#quizAnswers').html(html);
}

/* ghost text */
$(document).ready(function(){   
    $('.ghost-text').each(function(){
        var d = $(this).val();
        $(this).focus(function(){
            if ($(this).val() == d){
                $(this).val('').removeClass('ghost-text');
            }
        });
        $(this).blur(function(){
            if ($(this).val() == ''){
                $(this).val(d).addClass('ghost-text');
            }
        });
    });
});
/* end ghost text */