$(document).ready(function(){
	
});

function showSignupPrompt(showLogin){
	$('#overlay').fadeIn(200, function(){
		$(this).bind('click', closeLogin);
		$('#login-prompt').fadeIn(200, function(){
			if(showLogin){
				$('#login-prompt-parent').animate({marginLeft: -363}, 200, function(){
					$('#txtLoginEmail').focus();
				});
			}else{
				$('#login-prompt-parent').animate({marginLeft: 0}, 200, function(){
					$('#txtSignupName').focus();
				});
			}
		});
	});
}

function closeLogin(){
	$('#login-prompt').fadeOut(50, function(){
		$('#overlay').fadeOut(50);
	});
}

function showLoginPrompt(){
	showSignupPrompt(true);
}

function showEduConfirm(){
	$('#overlay').fadeIn(200, function(){
		$('#edu-email-confim').fadeIn(200, function(){
			
		});
	});
}

function closeConfirmEdu(){
	$('#edu-email-confim').fadeOut(50, function(){
		$('#overlay').fadeOut(50);
	});
}

/* ghost text */
$(document).ready(function(){   
    $('.ghost-text').each(function(){
        var d = $(this).val();
        $(this).focus(function(){
            if ($(this).val() == d){
                $(this).val('').removeClass('ghost-text');
            }
        });
        $(this).blur(function(){
            if ($(this).val() == ''){
                $(this).val(d).addClass('ghost-text');
            }
        });
    });
});
/* end ghost text */