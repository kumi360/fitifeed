$(document).ready(function () {
	$('#frmCrop').submit(cropImage);
	$('#cropImage').load(function(){
		initCrop();
	});
});
function initCrop(){
	$('#fiting').hide();
	$('#cropping').show();
	wd = $('#actualImage').width() / $('#cropImage').width();
	$('#shrinkRatio').val(wd);
	var def = 50;
	if($('#cropImage').width() < $('#cropImage').height())
		def = $('#cropImage').width();
	else
		def = $('#cropImage').height();
    $('#cropImage').imgAreaSelect({
    	  x1: 0, y1: 0, 
    	  x2: def, y2: def,
          aspectRatio: '1:1', 
          handles: true,
          onInit: resize,
          onSelectChange: resize
    });
}
function resize(img, selection){
    $('#preview').width(selection.width).height(selection.height);
    $('#imgPreview').css('margin-left', -selection.x1).css('margin-top', -selection.y1);
    $('#x1').val(selection.x1);
    $('#y1').val(selection.y1);
    $('#x2').val(selection.x2);
    $('#y2').val(selection.y2);
    $('#width').val(selection.width);
    $('#height').val(selection.height);
}
function cropImage(){
	$('#divCropData').hide();
	$('#divCroping').show();
	$('#imgText').attr('src', '');
	$.ajax({
		url:'',
		type: 'POST', 
		data: $('#frmCrop').serialize(),
		dataType: 'html', 
		success: function(data) {
		  if(data.split('|').length == 1){
		  	$('#btnStartFiti').val('go back to fiti');
		  	$('#cropping').hide();
		  	$('#fiting').show();
		  	$('#cropImage').imgAreaSelect({
		  		remove: true
		  	});
		  	$('#imgCroppedImage').unbind();
		  	$('#imgCroppedImage').attr('src', data);
		  	$('#imgCroppedImage').load(function(){
		  		$('.overlayImageLoading').css('margin-top', -2-$('#imgCroppedImage').height())
		  		.height($('#imgCroppedImage').height()+2)
		  		.width($('#imgCroppedImage').width()+2);
		  		$('#imgCroppedImage').unbind();
		  		modifyImage();
		  		/*applyFilters($('#filters a:first-child').attr('value'), function(data){
		  			modifyImage();
		  		});*/
		  	});
		  }else{
		  	alert('error: ' + data);
		  }
		  $('#divCroping').hide();
		  $('#divCropData').show();
		}
	});
    return false;
}
function applyFilters(filter, callback){
	showLoading();
	$('#baseImage').val(filter);
	$.ajax({
			url: '/applyFilter/' + filter,
			type: 'POST',
			dataType: 'html',
			success: function(data){
				$('#imgCroppedImage').attr('src', data);
				$('#imgCroppedImage').load(function(){
					$('#imgCroppedImage').unbind();
					$('#baseImage').val(filter);
					hideLoading();
					if(callback)
						callback(null, data);
				});
			}
	});
}
function showLoading(){
    $('.overlayImageLoading').slideDown(10);
}
function hideLoading(){
    $('.overlayImageLoading').slideUp(100);
}
function changeOrientation(){
	showLoading();
	data = {
		tw: $('#imgCroppedImage').width(),
		th: $('#imgCroppedImage').height(),
		x: 0,
		y: 0,
		text: $('#txtText').val(),
		font_id: $('input[name="chkFont"]:checked').val(),
		textDirection: $('input[name="chkOrient"]:checked').val()
	};
	getTextImage(data, function(err, data){
		if(!err){
			$('.overlayTextImage').css('margin-top', -$('#imgCroppedImage').height());
			$('#imgText').attr('src', data);
			hideLoading();
		}else{
			alert('error: ' + err);
		}
	});
}
function changeFont(){
	showLoading();
	data = {
		tw: $('#imgCroppedImage').width(),
		th: $('#imgCroppedImage').height(),
		x: 0,
		y: 0,
		text: $('#txtText').val(),
		font_id: $('input[name="chkFont"]:checked').val(),
		textDirection: $('input[name="chkOrient"]:checked').val()
	};
	getTextImage(data, function(err, data){
		if(!err){
			$('.overlayTextImage').css('margin-top', -$('#imgCroppedImage').height());
			$('#imgText').attr('src', data);
			hideLoading();
		}else{
			alert('error: ' + err);
		}
	});
}
function modifyImage(){
	showLoading();
	data = {
		tw: $('#imgCroppedImage').width(),
		th: $('#imgCroppedImage').height(),
		x: 0,
		y: 0,
		text: $('#txtText').val(),
		font_id: ($('input[name="chkFont"]:checked').val()?$('input[name="chkFont"]:checked').val():0),
		textDirection: $('input[name="chkOrient"]:checked').val()
	};
	getTextImage(data, function(err, data){
		if(!err){
			$('.overlayTextImage').css('margin-top', -$('#imgCroppedImage').height());
			$('#imgText').attr('src', data);
			hideLoading();
		}else{
			alert('error: ' + err);
		}
	});
}
function getTextImage(data, callback){
	$.ajax({
		type: 	'POST',
	  	url: "/getTextImage?r=_"+Math.floor(Math.random()*100000), //id filters here
	  	data: data,
	  	dataType: 'html',
	  	success: function(data) {
	  		//alert(data);
	  		if(data.split('|')[0] != 'error'){
		  		if(callback){
		  			callback(null, data.split('|')[1]);
		  		}
	  		}else{
	  			if(callback)
					callback(data.split('|')[1]);
	  		}
		},
		error: function(data){
			if(callback)
				callback(data);
		}
	});
}
function finalize(){
	$('#btnFinalize').hide();
	$('#divFinalizing').show();
	data = {
		width: $('#imgCroppedImage').width(),
		height: $('#imgCroppedImage').height(),
		text: $('#txtText').val(),
		sourceUrl: $('#sourceUrl').val(),
		sourceImage: $('#sourceImageUrl').val(),
		baseImage: $('#baseImage').val()
	};
	$.ajax({
		type: 	'POST',
	  	url: "/finalize?r=_"+Math.floor(Math.random()*100000),
	  	data: data,
	  	success: function(data) {
	  		$('#divFinalizing').hide();
	  		$('#final').html(data);
	  		$('#fiting').hide();
	  		$('#final').show();
	  		$('#btnFinalize').show();
		},
		error: function(data){
			//alert(data);
			$('#divFinalizing').hide();
			$('#btnFinalize').show();
		}
	});
}
function fitiAgain(){
	$('#final').hide();
	$('#fiting').show();
}
