//*var hostUrl = "https://fitifeed.com/public/"; //does not work on safari.. as the https redirect does not work*/
var hostUrl = "https://s3.amazonaws.com/cdn.fitifeed.com/";
var host = "http://fitifeed.com/";
//var hostUrl = "http://localhost:3000/public/";
//var host = "http://localhost:3000/";
var fitiHost = hostUrl + "fitibutton/";
closeFiti = function(){
	document.getElementById('fiti-bar').style['display'] = 'none';
	document.body.style["overflow"] = "auto";
	(elem=document.getElementById('fiti-bar')).parentNode.removeChild(elem);
	(elem=document.getElementById('fiti-button-css')).parentNode.removeChild(elem);
};
(function(){
console.log('Starting fiti button');
//Starting init button
if(document.getElementById('fiti-bar') == null){
	var h = document.getElementsByTagName("head")[0];
	var rnd = Math.floor(Math.random()*1000000000);
	
	console.log('downloading fitibutton.css');
	var css = document.createElement('link');
	css.type = 'text/css';
	css.rel = 'stylesheet';
	css.id = 'fiti-button-css';
	css.href = fitiHost + 'css/fitibutton.css?r=_'+rnd;
	h.appendChild(css);
	
	fitibar = document.createElement('div');
	fitibar.id = "fiti-bar";
	fitibar.className = "fiti";
	fitibar.setAttribute('style', 'display: block;');
	html = '<div class="content" style="display: block;">'+
				'<div class="fiti-head">'+
					'<div class="fiti-logo"></div>'+
					'<div class="fiti-close" onclick="closeFiti()">close</div>'+
					'<div style="clear: both;"></div>'+
				'</div>'+
				'<div class="fiti-images" id="fiti-images">'+
				'<div style="background: transparent url('+host+'public/img/ajaxloading-regular.gif) no-repeat; padding: 30px 0px 0px 30px;" />'+
				'</div>'+
			'</div>';
	fitibar.innerHTML = html;
	document.body.appendChild(fitibar);
	console.log('added fiti-bar');
	
	function init(){
		//curLocation = location.hostname;
		//alert(curLocation);
		source = location.hostname.replace('www.', '');
		//alert(source);
		html = "<div class='text'><h1>Which of these images would you like to FITI on?</h1></div>";
		imgs = document.getElementsByTagName("img");
		for(i=0; i<imgs.length; i++){
			curImg = imgs[i];
			width = curImg.clientWidth;
			height = curImg.clientHeight;
			if(width > 150 && height > 100 && curImg.src.length > 0){
				parent = curImg.parentNode;
				//alert(parent);
				//console.log(parent.getAttribute('href'));
				src = curImg.src;
				//google images fix
				if(parent.nodeName == 'A' && source == 'google.com'){
					ptrn = /imgurl=([^&]*)&/i;
					out = parent.getAttribute('href').match(ptrn);
					if(out != null)
						src = out[1];
				}
				if(source == 'pinterest.com'){
					src = src.replace('_b.', '_f.');
				}
				elm = "<div class='fiti-image'>"+
					"<div class='dim'>" + width + " x " + height + "</div>"+
					"<div><a href='" + host + "cropImage/"+encodeURIComponent(src)+"' target='_blank'>" +
					"<img src='" + curImg.src +"' /></a></div>"+
					"</div>";
				html = html + elm;
			}
		}
		html = html + "<div style='clear: both;'></div>";
		document.getElementById('fiti-images').innerHTML = html;
		document.getElementById('fiti-bar').addEventListener('click', function(){
			//closeFiti();
		}, false);
		document.body.style["overflow"] = "hidden";
	}
	function getSelectedText(){
	  var t = '';
	  if(window.getSelection){
	    t = window.getSelection();
	  }else if(document.getSelection){
	    t = document.getSelection();
	  }else if(document.selection){
	    t = document.selection.createRange().text;
	  }
	  return t;
	}
	init();
}else{
	closeFiti();
}
})();

