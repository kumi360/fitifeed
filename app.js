// modules
var express = module.express = require("express");
var util = require("util");
var mongoose = module.mongoose = require("mongoose");
var mongoose_types = module.mongoose_types = require("mongoose-types");
var fs = require('fs');
var env = module.env;
var crypto = require("crypto");

//Environment
/* *********  Set the environment here ********************/
require('./env');
// init
var options = module.certOptions = {};
var app = module.app = express.createServer();
app.settings.env = module.env;
//Configs
require('./config');
var appHttps = module.appHttps;
if(module.env == 'production'){
	module.certOptions = {
	  key:  fs.readFileSync(module.sslKey).toString(),
	  cert: fs.readFileSync(module.sslCert).toString()
	};
	//Create and start https server
	appHttps = module.appHttps = express.createServer(module.certOptions);
	
	//Create https routes
	require('./https_routes');
	appHttps.listen(module.httpsPort);
	console.log("starting https server on: " + module.httpsPort);
}

process.on("uncaughtException", function (e) {
	console.log("*****************************************************");
	console.log("stack: " + e.stack);
	console.log("*****************************************************");
	/*console.log("Error is %j", e); 
    console.log("Error is %j", e.message ); 
  	console.log("Error is %j", util.inspect(e, true) );*/ 
	/*console.log("Caught Exception: " + error);
	console.log("Caught Exception: " + error);*/
});

console.log('Environment: ' + module.env);
app.settings.env = module.env;

//Set http port
var port = module.httpPort;


// Load all the controllers
var controllers = module.controllers = new Array;
var fs = require("fs");
fs.readdirSync(__dirname + '/controllers').forEach(function(file){
	//console.log(file.isDirectory());
	if(file.indexOf('.') != -1){
		curFile = file.replace('.js', '');
		if(curFile != 'index'){
			controllers[curFile] = require(__dirname + '/controllers/' + file);
		}
	}
});

// Load all the models
var models = module.models = new Array;
require("fs").readdirSync(__dirname + '/models').forEach(function(file){
	curFile = file.replace('.js', '');
	if(curFile != 'index'){
		models[curFile] = require(__dirname + '/models/' + file);
	}
});

//routes
require('./http_routes');

console.log("starting http server on: " + module.httpPort);
// main
app.listen(module.httpPort);
